package com.train.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * This class take object of any type.
 * And return all information about this instance.
 */
public class Agent {
    private Object object;

    public Agent(Object object) {
        this.object = object;
    }

    public String getSecretInfo() throws ClassNotFoundException {
        Class cls = Class.forName(object.getClass().getCanonicalName());
        StringBuilder info = new StringBuilder();
        info.append("Class name: ").append(cls.getName());
        Field[] fields = cls.getDeclaredFields();
        info.append("\nFields:\n");
        for (Field field : fields) {
            info.append(field).append("\n");
        }
        Constructor[] constructors = cls.getConstructors();
        info.append("Constructors:\n");
        for (Constructor constructor : constructors) {
            info.append(constructors).append("\n");
        }
        Method[] methods = cls.getDeclaredMethods();
        info.append("Methods:\n");
        for (Method method : methods) {
            info.append(method).append("\n");
        }
        return info.toString();
    }
}
