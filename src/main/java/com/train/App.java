package com.train;

import com.train.model.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;

public class App {
    private static final Logger logger = LogManager.getLogger("InfoForUser");

    public static void main(String[] args) {
        Person person = new Person("Arch", "Dain", 20);
        Controller controller = new Controller(person);
        MyView myView = new MyView(controller);
        try {
            myView.printFields();
            myView.invokeMethods();
            myView.setValueNotKnowingType();
            myView.shoeSecretInfo(person);
        } catch (ClassNotFoundException e) {
            logger.info("Can`t find the class!!!");
        } catch (NoSuchMethodException e) {
            logger.info("Can`t find method.");
        } catch (IllegalAccessException e) {
            logger.info("Can`t access method.");
        } catch (InvocationTargetException e) {
            logger.info("Fail(");
        } catch (NoSuchFieldException e) {
            logger.info("Set true to field access");
        }
    }
}
