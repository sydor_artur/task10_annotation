package com.train;

import com.train.model.Agent;
import com.train.model.Person;

import java.lang.reflect.Field;

public class Controller {
    private Person person;

    public Controller(Person person) {
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }

    public Field[] getFieldsOfObject() {
        return person.getClass().getDeclaredFields();
    }

    public void changeValueOfFields() throws NoSuchFieldException, IllegalAccessException {
        Field name = person.getClass().getDeclaredField("name");
        Field lastName = person.getClass().getDeclaredField("lastName");
        Field age = person.getClass().getDeclaredField("age");
        name.setAccessible(true);
        lastName.setAccessible(true);
        age.setAccessible(true);
        name.set(person, "Rick");
        lastName.set(person, "Rodes");
        age.set(person, 30);
    }

    public String getSecretInfo(Object object) throws ClassNotFoundException {
        return new Agent(object).getSecretInfo();
    }
}
