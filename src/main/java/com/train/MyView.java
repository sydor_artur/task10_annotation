package com.train;

import com.train.model.Person;
import com.train.model.PrintPermition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MyView {
    private static final Logger logger = LogManager.getLogger("InfoForUser");
    private Controller controller;

    public MyView(Controller controller) {
        this.controller = controller;
    }

    public void printFields() throws ClassNotFoundException {
        Class cl = Class.forName(controller.getPerson().getClass().getCanonicalName());
        Field[] fields = cl.getDeclaredFields();
        logger.info("Fields of class - " + cl.getName());
        for (Field field : fields) {
            if (field.isAnnotationPresent(PrintPermition.class)) {
                PrintPermition value = field.getAnnotation(PrintPermition.class);
                logger.info("Annotation value: " + value.permition());
                logger.info(field);
            }
        }
    }

    public void invokeMethods() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        //no parameters
        Class[] noParam = {};
        //String parameter
        Class[] stringParameters = {String.class};
        //Integer parameter
        Class[] integerParameter = {Integer.TYPE};
        //String and Integer parameters
        Class[] mixedParameters = {String.class, int[].class};
        //String array
        Class[] stringArray = {String[].class};


        Person object = controller.getPerson();
        Class cl = Class.forName(object.getClass().getCanonicalName());
        Method method;

        logger.info("----------------------------------");
        logger.info("Invoke method through reflection");
        method = cl.getDeclaredMethod("howOld", noParam);
        logger.info(method.invoke(object, null));
        method = cl.getDeclaredMethod("isEnough", integerParameter);
        logger.info("Is adult? - " + method.invoke(object, object.getAge()));
        method = cl.getDeclaredMethod("myMethod", mixedParameters);
        logger.info(method.invoke(object, object.getName(), new int[]{3, 7, 2000}));
        method = cl.getDeclaredMethod("myMethod", stringArray);
        String[] args = {object.getName(), object.getLastName()};
        logger.info(method.invoke(object, (Object) args));
    }

    public void setValueNotKnowingType() throws IllegalAccessException, NoSuchFieldException {
        logger.info("----------------------------------");
        logger.info("Set value into field not knowing its type");
        logger.info("Before changing");
        printAllFields();
        controller.changeValueOfFields();
        logger.info("After changing");
        printAllFields();
    }

    public void printAllFields() throws IllegalAccessException {
        Object object = controller.getPerson();
        for (Field field : controller.getFieldsOfObject()) {
            field.setAccessible(true);
            logger.info(field.getName() + " - " + field.get(object));
        }
    }

    public void shoeSecretInfo(Object object) throws ClassNotFoundException {
        logger.info(controller.getSecretInfo(object));
    }
}
