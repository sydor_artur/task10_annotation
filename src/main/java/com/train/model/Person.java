package com.train.model;

/**
 * Class for my annotation testing.
 */
public class Person {
    @PrintPermition
    private String name;
    @PrintPermition
    private String lastName;
    private int age;

    public Person(String name, String lastName, int age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String howOld() {
        return "Your age is " + age;
    }

    public boolean isEnough(int age) {
        return age >= 18;
    }

    public String myMethod(String a, int... args) {
        String info = "Name: " + a + "; Day of birth: ";
        for (int numbers : args) {
            info += numbers + " ";
        }
        return info;
    }

    public String myMethod(String... args) {
        String fullName = "Full name: ";
        for (String s : args) {
            fullName += s + " ";
        }
        return fullName;
    }
}
